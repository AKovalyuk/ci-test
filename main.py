from fastapi import FastAPI, status
from pydantic import BaseModel


class SumRequest(BaseModel):
    nums: list[int]


class SumResponse(BaseModel):
    result: int


def logic_sum(nums: list[int]) -> int:
    return sum(nums)


app = FastAPI()


@app.get(
    path='/sum',
    status_code=status.HTTP_200_OK,
)
async def sum_view(request: SumRequest):
    return SumResponse(result=logic_sum(request.nums))
